menu_item: true


# Android apps for security and anonimity

**Warning:** Most of these links link to the clearweb!  

To Make an android phone a bit more secure I start with a clean install of [LineageOS](https://lineageos.org).
Because some apps don't play well without google's play services, I like to get the [MicroG version of LineageOS](https://lineage.microg.org).
[MicroG](https://microg.org) tries to emulate parts of the google play services.  

A non-Android option includes the opensource [Pinephone](https://www.pine64.org/pinephone). The Pinephone runs mainline linux!
It's a great phone and I recommend you check it out! But beware, it's very alpha quality hardware and software ;)

### App Stores
- [F-Droid](https://f-droid.org) | The must-have opensource appstore.
- [Aurora Store](https://auroraoss.com/download/AuroraStore) | [F-Droid](https://f-droid.org/en/packages/com.aurora.store) | Download apps from the Google Play Store. Supports logins with an anonymous account.  

### Personal Cloud and Syncing
I self host a [Nextcloud](https://nextcloud.com) instance, this enables me to synchronise file, calendars and contacts without the need for google or any third party.
To do all this, it uses open protocols like: WebDAV, CalDAV and CardDAV.  
There are a lot of android apps that can talk with a nextcloud instance:  

- [Nextcloud](https://nextcloud.com/install/#install-clients) | [F-Droid](https://f-droid.org/packages/com.nextcloud.client) | The official nextcloud app enables file sync over the WebDAV protocol.  
- [DAVx5](https://www.davx5.com) | [F-Droid](https://f-droid.org/en/packages/at.bitfire.davdroid) | Enables calendar and contacts syncing over CalDAV and CardDAV.  
- [KeePassDX](https://www.keepassdx.com) | [F-Droid](https://f-droid.org/en/packages/com.kunzisoft.keepass.libre) | Save and use passwords, keys and digital identities in a secure way. Since it uses a file based local database, you can sync it using the nextcloud app.  


### Cryptocurencies
- [Altcoin prices](https://altcoinprices.uwot.eu) | Monitor your crypto assets.

### Utilities
- [FreeOTP](https://freeotp.github.io) | [F-Droid](https://f-droid.org/en/packages/org.fedorahosted.freeotp) | A two-factor authentication application for systems utilizing one-time password protocols.  
- [KISS Launcher](https://kisslauncher.com) | [F-Droid](https://f-droid.org/en/packages/fr.neamar.kiss) | A simple and light replacement launcher for the default launcher.  
- [Slide](https://github.com/ccrama/Slide) | [F-Droid](https://f-droid.org/app/me.ccrama.redditslide) | A reddit client
- [Notally](https://github.com/OmGodse/Notally) | [F-Droid](https://f-droid.org/packages/com.omgodse.notally) | A simple and great note taking app.

### Maps and RoutePlanners
- [OSMAnd](https://osmand.net) | [F-Droid](https://f-droid.org/en/packages/net.osmand.plus) | Global Mobile Map Viewing and Navigation for Online and Offline OSM Maps
- [Transportr](https://transportr.app) | [F-Droid](https://f-droid.org/packages/de.grobox.liberario) | A transportation app that has support for local transportation APIs

### Security
- [Mullvad VPN](https://mullvad.net) | [Onion](http://o54hon2e2vj6c7m3aqqu6uyece65by3vgoxxhlqlsvkmacw6a7m7kiad.onion) | [F-Droid](https://f-droid.org/en/packages/net.mullvad.mullvadvpn) | The opensource app for the mullvad VPN service
- [Tor Browser](https://www.torproject.org/download/#android) | To install the tor browser via F-Droid you need to [enable the guardian repo](https://guardianproject.info/fdroid), more info [here](https://support.torproject.org/tormobile/tormobile-7).

### Multimedia
- [Open camera](https://opencamera.org.uk) | [F-Droid](https://f-droid.org/en/packages/net.sourceforge.opencamera) | A replacement for the default camera app
- [NewPipe](https://newpipe.net) | [F-Droid](https://f-droid.org/en/packages/org.schabi.newpipe) | A youtube player for android that doesn't require you to log in.  
- [RadioDroid](https://github.com/segler-alex/RadioDroid) | [F-Droid](https://f-droid.org/packages/net.programmierecke.radiodroid2) | A radio streaming app.
- [AntennaPod](https://antennapod.org) | [F-Droid](https://f-droid.org/packages/de.danoeh.antennapod) | A great podcast client.

### Instant Messaging
- [Telegram FOSS](https://github.com/Telegram-FOSS-Team/Telegram-FOSS) | [F-Droid](https://f-droid.org/app/org.telegram.messenger) | The FOSS version of the telegram IM client.
- [Element](https://element.io) | [F-Droid](https://f-droid.org/en/packages/im.vector.app) | A client for the secure decentralized [Matrix](https://matrix.org) network.  

