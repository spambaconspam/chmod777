menu_item: true


# Onion Links

### Search Engines
- [DuckDuckGo](https://duckduckgogg42xjoc72x3sjasowoarfbgcmvfimaftt6twagswzczad.onion) | The onion version of the DuckDuckGo search engine

### Podcasts
- [Darknet Diaries](http://uka5ybpmh3u54dkv.onion) | True stories from the dark side of the Internet.  

### News
- [Tape](http://tape6m4x7swc7lwx2n2wtyccu4lt2qyahgwinx563gqfzeedn5nb4gid.onion) | News from the darknet
- [Flashlight](http://ovgl57qc3a5abwqgdhdtssvmydr6f6mjz6ey23thwy63pmbxqmi45iid.onion) | Info beam into the darkweb

### Social
- [Dread](http://dreadytofatroptsdj6io7l3xptbet6onoyno2yv7jicoxknyazubrad.onion) | A reddit like site, also check [this subdread](http://dreadytofatroptsdj6io7l3xptbet6onoyno2yv7jicoxknyazubrad.onion/d/onions) version of the [clearnet onions subreddit](https://www.reddit.com/r/onions)

### Onion Directories
- [Darknet Live](http://darkzzx4avcsuofgfez5zq75cqc4mprjvfqywo45dfcaxrwqg6qrlfid.onion) | Darknet news and a large onion link directory
- [Dark Eye](http://darkeyepxw7cuu2cppnjlgqaav6j42gyt43clcn4vjjf7llfyly5cxid.onion)

### Radio
- [Deep Web Radio](http://anonyradixhkgh5myfrkarggfnmdzzhhcgoy2v66uf7sml27to5n2tid.onion) | Deep web radio station

### Cryptocurencies
- [Monero](http://monerotoruzizulg5ttgat2emf4d6fbmiea25detrmmy7erypseyteyd.onion) | Private, decentralized cryptocurrency that keeps your finances confidential and secure

### Blogs
- [Dark Tech - High Life](http://2cndnuufyzgwkcct.onion) 
- [S-Config](http://xjfbpuj56rdazx4iolylxplbvyft2onuerjeimlcqwaihp3s6r4xebqd.onion)
- [Rustic Cyberpunk](http://kpz62k4pnyh5g5t2efecabkywt2aiwcnqylthqyywilqgxeiipen5xid.onion)

