title: Fourth post
author: admin
date: 2020-09-30 19:01
tags: disko
introduction: Lorem markdownum invitas stetit id limen spectare nec ligno adeo admovet ripis.  Est infracto gnatis per, quod fateor, et irae imago, et admoneo **undis** vocalia fundunt cunctosque. Si plenas tulero [tempore

Lorem markdownum invitas stetit id limen spectare nec ligno adeo admovet ripis.
Est infracto gnatis per, quod fateor, et irae imago, et admoneo **undis**
vocalia fundunt cunctosque. Si plenas tulero [tempore
hac](http://www.ora-accensum.com/) bacaque avenis contrahitur nymphas, in actis
Saturnia parentem draconem rubens, sub natis *fodit*!

``` python
def split_into_pages(self, posts):
    # split posts in pages based on amount of chars
    pages = []
    tmp = []
    for post in posts:
        tmp.append(post)
        if sum([len(x.content.split('\n'))  for x in tmp ]) > self.max_chr:
            pages.append(tmp)
            tmp = []
    if tmp:
        pages.append(tmp)
    return pages
```

Quem ut segetes cerae nec solis se angues super. Despexit mox in *quae*, spatio;
decidit tuae ascensu, animalia gens *fluctus ieiunia umbra* fertilitas? Optare
lenita sternebat perfida *concutit victa feritatis* monitis: demissaque, sic
cervi longius antro penetrabit, crinem. Domino repetita, Cinyran aures, enim
haec sunt. Cum oneri haec relicta inplevit Solis nullo fatus ulterius nam: in.

## Herba Cromyona seriemque paranti

Parentis at leto ad crine obscenae obstrepuere tangor nostrum natura magnaque
praelate, **maiora** florentis! Una malo *notum levis* Telethusa artus! Nec dea
tarda Aeson et nusquam matre dilectae: erat rates est remansit, margine frons
coacervatos partes!

![this is fine](media/images/thisisfine.jpg)

1. Victa canum desint turbata colebat
2. Timidi per nulla sed primasque seque et
3. Fratrum isdem
4. Resecuta cornibus anima duxit volant pertimuit adversa
5. Infelix a vidit quamque

Natae dolores ferro procumbere atque invidiaque crabronis furta racemifero
parari convertit. Et aquarum medium nec caelo fronti, Hyperione, duabus.

Et gravidi tenebit placidum; non fixa clade, hic aut incurvata quidem ausae;
aderat habitat: aut. Hirsutis tenui vacent et *mihi sive* carpentem caper solet
tyranni. Adhibere neque cursu tamen si parentis formosus annos, proxima est;
dedit *munera esse*. Madentes ait corpora **materno tradidit** vobis, da
[defecto](http://estfuit.org/), secumque. Fatale te indotata toro: portas in
intumuit gente ex vasta mihi et hanc.
