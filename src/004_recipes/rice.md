# Brown organic rice

    1       cup     brown round rice
    2       cup     water
    0.5     tsp     salt

Pressure cook 15 minutes, let depressurize naturally.  


# Jeera rice

    1       cup     long grain basmati rice
    1.5     cup     water
    1       tsp     cumin seeds
    1               black cardamom
    2               bay leaves
    1       inch    cinamon
    3               dried red chillis
    0.5     tsp     salt

    fresh coriander leaves

Bake all dried ingredients.  
Add water, rice and salt and mix well.  
Let pressure cooker pressurize and immediately turn off gas.  
Wait till cooker depressurizes.  
Mix in fresh coriander.  


