# Dal soup

    1   cup     red split lentils  
    2.5 cup     water  
    0.5 tsp     turmeric  

    1   tsp     ghee  
    2           green chillis
    2   tsp     cumin seeds  
    2   tsp     black mustard seeds  
    1.5 tsp     ginger garlic paste  
    1           onion  

    1.5 tsp     salt
                coriander leaves
    0.5 tsp     red chilli powder

Cook until lentils are falling appart  
    - lentils  
    - water  
    - turmeric  

Bake chillis in ghee then add cumin seeds untill they pop.  

Add and bake until done:  
    - ginger garlic paste  
    - onion  

Add and cook:  
    - dal  
    - salt  
    - coriander leaves  


# Pressure cooker dal

    0.5     cup     chana dal
    0.5     cup     mung dal
    1.5     cup     water
    0.25    tsp     turmeric
    1       tsp     salt

    1       tsp     cumin seeds
    1       tsp     black mustard seeds

    1       tsp     red chilli powder
    1       tbsp    garlic/ginger paste
    1               red onion diced

    1               tomato diced

    coriander leaves

Cook in pressure cooker for 5 wistles:  
- dal
- water
- turmeric
- salt

Bake in ghee or oil:  
- cumin seeds
- black mustard seeds

Add onion and cook till done.  

Add and cook till tomatos are mushy:  
- red chilli powder
- garlic/ginger paste
- tomato

Add dal and a bit of water and cook for a bit.  
Add coriander leaves.  




