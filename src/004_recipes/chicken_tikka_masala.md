# Paneer tikka masala

marinade:  

    1       cup     yoghurt
    1       tsp     oil
    1/4     tsp     ajwain
    1       tbsp    red chilli powder
    1/4     tsp     turmeric powder
    1/2     tsp     garam masala
    1       tbsp    ginger/garlic paste 
    1/2     tsp     coriander powder
    1/2     tsp     jeera powder
    1/2     tsp     salt

    1               large pack veg chicken pieces

gravy:

    1       tsp     cumin seeds
    3               chopped red onions
    1       tsp     ginger/garlic paste

    1/2     tsp     turmeric powder
    1       tsp     red chilli powder
    1/2     tsp     coriander powder
    1/2     tsp     cumin powder

    2               chopped tomatos or puree
    1/2     cup     water
    1/2     tsp     salt

    1       cup     green peas

    1/2     tsp     garam masala
    1/2     tsp     fenugreek leaves


Mix all ingredients in marinade.  
Add veg chicken and leave for about 20 minutes.  
Bake chicken until golden.  


Bake cumin seeds and add and bake red onions.  
Add ginger/garlic paste.  
Add and bake for a few seconds on low flame:  
    - turmeric powder  
    - red chilli powder  
    - coriander powder  
    - cumin powder  

Add tomato puree and cook until oil separates.  
Add some and bring to a boil:  
    - water  
    - salt  
    - green peas  

Add veg chicken and cook with lid on, for 10 minutes on low flame.  

Add garam masala and fenugreek.  

Turn off flame and leave the pan covered for 5 minutes.  
