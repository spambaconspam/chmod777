https://www.youtube.com/watch?v=Jxw4wVqHVM4&list=PLTM6KhwKNVk5sshsISvntDpiyFzy3u2br&index=35

# Gatte Ki Sabzi (chickpea cuttlets masala)

## Ingredients
### Gatta

    1     cup     chickpea flour  
    1/4   tsp     hing  
    3     tbsp    oil  
    1/2   tsp     carom seeds (ajwain)  
    1/4   tsp     turmeric powder  
    1/3   tsp     red chilli powder  
    1/4   tsp     cumin powder  
    1/4   tsp     coriander powder  
    1/4   tsp     garam masala  
    1/3   tsp     salt  

### Curry

    1/2   cup     onion paste (2 onions)  
    1/3   cup     tomato puree (1 tomatos)  
    3     tbsp    yoghurt  
                  coriander leaves  
    1     tbsp    ginger/garlic paste  
    4     tbsp    oil  
    1/2   tsp     cumin seeds  
    1/2   tsp     turmeric powder  
    1     tsp     red chilli powder  
    1/2   tsp     cumin powder  
    1/2   tsp     coriander powder  
    1/2   tsp     garam masala powder  
    1/2   tsp     salt  

## Preparation
### Gatta
Mix all ingredients save the oil in a bowl.  
Add oil and mix again.  
Add a little bit of water and form a stiff dough.  
Make 4 rolls and boil in 1/2 liter of water for about 14 minutes.  
Slice rolls to form the gatte, save water for later.  

### Curry
Bake cumin in oil.  

On low heat bake:  
- turmeric powder  
- red chilli powder  
- cumin powder
- coriander powder

On medium heat with covered pan, add and bake for a couple of minutes:  
- onion paste  
- ginger/garlic paste  

Add tomato puree and cook in covered pan, until oil seperates (+- 2 minutes)

Let masala cool down and add yoghurd and mix well.  

on medium heat cook for a minute.  

Add and cook for 4 minutes, with covered pan:  
- gatta water  
- salt  
- gatta  

Add:  
- garam masala  
- coriander leaves  

Leave it for a couple of minutes before serving with coriander leafs
