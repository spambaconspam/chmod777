# Lasooni Palak Khichdi

## Ingredients:

    KIDCHI
    1/2     cup     mung dal
    1       cup     basmati rice
    1/2     tsp     haldi
    1/2     tsp     salt

    SPINACH
    400     g       fresh spinach (or 200g frozen)
    3       tbsp    fresh mint leaves
    3       tbsp    fresh coriander leaves
    3               green chillis
    3       cloves  garlic

    TADKA #1
    1       tbsp    ghee
    1       tsp     jeera seeds
    1/2     tsp     hing
    2       tbsp    ginger garlic paste
    2               red chillis

    1               chopped red onion
    1               chopped tomato
    1/2     tsp     salt
    1       tbsp    coriander powder
    1       tsp     jeera powder
    1       tsp     garam masala

    1       tsp     lemon juice

    TADKA#2
    1       tbsp    ghee
    5       cloves  garlic in chunks
    1/2     tsp     hing
    3               red chillis
    1/4     tsp     cashmiri chilli powder


## Method:

Wash and soak dal and rice for 30 minutes.  
Strain and put in pressure cooker with 2 inches of water.  
Add salt and haldi.  
Add a little oil to prevent foaming, don't stir after this step.  
Pressure cook for one whistle.  

Blanch spinach and submerge in ice cold water to retain green color.  
Add and mix to a fine paste:  
- mint  
- coriander  
- green chillis  
- garlic  

Bake in ghee on medium flame:
- jeera seeds  
- hing  
- red chillis  
- ginger/garlic paste  

Add onion and cook until translucent.  

Add and cook on medium heat for 3 minutes:  
(add a bit of water to keep spices from burning)
- tomato  
- salt  
- coriander powder  
- jeera powder  
- garam masala  

Add spinach and cook for 4 minutes on medium flame.  
Add kidchi and cook for 4 minutes on medium flame.  

Add lemon juice

tadka #2
Bake garlic in ghee until golden brown.  
Add and bake for a couple of seconds:
- hing  
- red chillis  
Add cashmiri chilli powder  
Pour tadka over kidchi.  
