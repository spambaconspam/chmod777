link: https://www.youtube.com/watch?v=-gupWpTsF88

# Stuffed onion sabji

## Ingredients

Onion mix

    15              Small red onions
    1/4     tsp     turmeric
    1/2     tsp     chilli powder
    1/2     tsp     garam masala
    1/2     tsp     salt

Gravy

    2       tbsp    oil
    3/4     tsp     mustard seeds
    1/2     tsp     cumin seeds
    1/4     tsp     hing
    8               curry leaves

    1               onion filely chopped
    1.5     tbsp    ginger/garlic paste

    1/4     tsp     turmeric
    1       tsp     red chilli powder
    1/2     tsp     cumin powder
    1/2     tsp     coriander powder
    1/2     tsp     garam masala
    1/2     tsp     salt

    1               green bell peper cubed
    1.5     cup     tomato puree from fresh tomatos
    1/4     cup     whisked yoghurd

    1/2     tsp     kasuri methi

## Method

Cut a cross in the onions.  
Add all spices, mix well and let it rest for at least 15 minutes.  

Bake in oil:  
    - mustard seeds  
    - cumin seeds  
    - hing  
    - curry leaves  

Add and bake onions.  
Add and bake ginger/garlic paste.  

Add and saute on low flame:  
    - turmeric  
    - red chilli powder  
    - cumin powder  
    - coriander powder 
    - garam masala  
    - salt  

Add bell peper and bake for one minute.  
Add tomato puree and cook for 5 minutes until the oil separates.  

Turn off flame and add yoghurd, mix well.  
On low flame bake until oil separates.  

Add a bit of water to adjust consistency.  

Add and simmer onions in a covered pan for 8 minutes, or until onions are soft.  

Add kasuri methi








