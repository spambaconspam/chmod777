# Egg Biryani

## Ingredients

### fried onions
- 2     tbsp    oil
- 1             large onion sliced

### Egg mix:
- 5             hard boiled eggs
- 1     tbsp    oil
- 1/4   tsp     turmeric powder  
- 1/2   tsp     red chilli powder
- 1/4   tsp     salt

### Curd mix
- 1/2   cup     curd  
- 1     tbsp    red chilli powder  
- 1/4   tsp     turmeric powder  
- 1     tsp     coriander powder  
- 1/2   tsp     garam masala powder  

### Biryani
- 1     tbsp    oil  
- 2     tbsp    ghee  
- 1/2   tsp     cumin seeds  
- 2             bay leaves  
- 1     inch    cinamon stick  
- 1             star anice  
- 4             cloves  
- 3             cardamom

- 1             onion sliced
- 2             green chillis  
- 2             tomatos finely chopped  
- 1     tsp     ginger garlic paste  

- 2     tbsp    mint leaves
- 3     tbsp    coriander leaves


- 1     cup     basmati rice soaked for 20 minutes  
- 1.5   cup     water 
- 1.5   tsp     salt



## Method

### onions
Fry onion on high flame untill golden brown and set aside.  

### eggs
In same pan add on low flame:  
- oil
- turmeric  
- red chilli powder  
- salt  

Slit the eggs so they'll absorb more of the spices.  
Bake eggs on low flame in spice mix for about 2 minutes and set aside.  

### curd mix
Mix together in a separate bowl and set aside:  
- curd  
- red chilli powder  
- turmeric powder  
- coriander powder  
- garam masala powder  

### biryani
In pressure cooker bake in oil and ghee:
- cumin seeds  
- bay leaves  
- cinamon stick  
- star anice  
- cloves  
- cardamom

Add sliced onions and bake till done.  
Add and bake:
- green chillis
- tomatos  
- ginger/garlic paste

Turn off flame and add curd mix and cook untill oil separates.  

Add and mix:  
- mint leaves  
- coriander leaves  

Add layer:  
- soaked rice  
- water  

Add layer:  
- fried onions  
- eggs  
- coriander leaves  
- mint leaves  
- salt

if using pressure cooker:  
Close pressure cooker and cook for one whistle on high flame.  
Then cook on medium to low flame and switch off flame just before second whistle to keep pressure in.  
Let pan rest for 10 minutes before opening.  

if using pan:
Bring to a boil.  
Cover pan and cook for 20 minutes on low flame.  
Let pan rest for 10 minutes before opening.  

Serve with raita  












