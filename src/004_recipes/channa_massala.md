# Chana Masala

CHICKPEAS MIX  
- 200   g       dry chickpeas, soaked for 10 hrs  
-   1   bag     tea  
- 0.5   tsp     baking soda (**baking soda is not baking powder!**)  
-   2   tsp     salt  
-   3           cloves  
-   1   stick   cinamon  
-   1           whole cardamom

SPICE MIX  
-   2   tbsp    ghee
-   2           green chillis  
- 3.5   tsp     coriander powder  
-   1   tsp     ginger powder
-   1   tsp     red chilli powder  
- 1/2   tsp     garam masala powder  
-   2   tsp     pomegranade seed powder  
- 1/2   tsp     turmeric powder  
- 1/2   tsp     cumin powder  

MASSALA MIX  
- 1/8   tsp     asafoetida powder  
-   1           onion mashed into puree  
- 1/4   cup     yoghurd  
-   1   tsp     ginger garlic paste  
-   1   tbsp    tomato puree  
- 1/2   cup     boiling water  

- 1/2   tsp     dried fenugreek leaves  

Put chickpeas in pan with enough water to cover them. Cook with all ingredients from chickpea mix for about 40 minutes.  

Heat up ghee and bake the chillis.  
Add in the spice mix and bake for 30 seconds, now add the spices to the chickpeas and set aside.  

Bake in oil until onions are cooked:
    - asafoetida
    - onions

Add yoghurd and cook for 5 minutes.  

Add and cook until oil separates:  
    - ginger garlic paste  
    - tomato puree  

Add and cook on low flame for 15 minutes:  
    - water  
    - chickpeas
    - fenugreek  



# 2 quick method

- 2 ts channa massala kruiden  
- 2 blikjes kikkererwten  
- .5 blikje tomatenblokjes  
- 6 kleine rode uien  
- knoflook  
- .5 blikje cocos melk  

Bak gesneden uien en knoflook.  
Voeg kruiden toe en bak even mee.  
Voeg tomaten toe en bak 5 minuten.  
voeg kikkererwten en cocos melk toe en kook tot bijna droog.  

