# Ghee

# Ingredients

    2   blocks  butter
    strainer

    Use a non-black steel pan with a thick bottom.  
    This ensures that you can see the color of the milk solids which stick to the bottom.  

# Method

During the whole process the ghee needs to be stirred frequently without touching the bottom of the pan.  

Heat up the butter on medium-high flame.  
As soon as the ghee begins to boil, turn flame to medium-low.  
The bubbels will start small and will become bigger within a couple of minutes.  
When the ghee starts to clear up, the foam will mostly disappear and the milk solids will sink to the bottom.  
Turn off heat when the milk solids turn slightly brownish.  
Strain ghee into a jar.  
