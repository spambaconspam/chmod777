# Poha

For 2 servings:  

- 100   g   poha  
-  50   g   peanuts  
-   2       red onions diced  
-   3       green chilli sliced through middle  
-  10       curry leaves
- 1/2   ts  cumin seeds
- 1/2   ts  mustard seeds
- 1/2   ts  sugar
- 1/2   ts  turmeric
-           salt
-           oil
-           fresh coriander
-   2       lemons
-           dat knapperige spul

Spoel poha af in vergiet onder de kraan en laat 10 minuten rusten.  

Bak 30 seconden in laagje olie:  
- cumin  
- mustard  
- chillis  
- curry leaves  
- sugar  

Voeg 3/4 van de uien toe, bewaar een beetje als topping, en bak totdat uien gaar zijn.  

voeg toe en kook 1 minuut met deksel op de pan:
- turmeric  
- poha  
- zout  

voeg toe en kook 1 minuut met deksel op de pan:  
- sap van halve limoen  
- coriander  
- pinda's  

serveer met limoen sap, rode ui en knapperige dingetjes  
