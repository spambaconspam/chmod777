https://www.youtube.com/watch?v=U0PJJxU4fJ0

# Kaju masala

## Ingredients

    Cashew tomato paste:
    1       tbsp    ghee
    1/2     cup     cashews
    1/2     tsp     red chilli powder
    2               onions sliced
    3               tomatos diced
    2       tsp     ginger/garlic paste
    1/2     cup     water

    Gravy:
    1       tbsp    ghee
    2               green cardamom
    3               cloves
    1               1 inch cinamon stick
    1       tsp     cumin seeds
    1       cup     cashew
    1       tbsp    red chilli powder
    1       tbsp    coriander powder
    1       tsp     fennel powder

                    cashew tomato paste
    1.5     tsp     salt
    1       cup     hot water
    1       tsp     kasuri methi/fenugreek leafs
    1       tsp     garam masala
    1       tbsp    fresh cream
                    chopped fresh coriander leafs

## Preparation

cashew tomato paste:  
Bake cashews in ghee until golden brown.    
Add and cook untill tomatoes and onions are done:  
    - red chilli powder  
    - onions  
    - tomatos  
    - ginger/garlic paste  

Cool down and grind to fine paste, add water to adjust consistancy.  


gravy:  
Bake in ghee:  
    - cardamom  
    - cloves  
    - cinamon  
    - cumin seeds  

Add cashews and bake untill golden brown.  

Add and bake for a minute:  
    - red chilli powder  
    - coriander powder  
    - fennel powder  

Add and cook until oil separates (7/8 minutes):  
    It is ok to add a little watter but not too much since we have to cook it down.  
    - cashew tomato paste  
    - salt  

Add hot water to adjus consistency.  

Add and mix well:  
    - fenugreek leafs  
    - garam masala  
    - fresh cream    
    - chopped coriander leafs    
