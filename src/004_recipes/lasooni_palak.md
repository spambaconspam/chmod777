# Lasooni palak

link: https://www.youtube.com/watch?v=BZzHpN-alP4&list=PLTM6KhwKNVk6QXtZFfbqWmkBbb3pC-Q7p&index=16

## Ingredients

    500     g       spinach
    2       tbsp    oil     

    1       tsp     cumin seeds
    2               green chillis chopped
    1               onion finely chopped
    2       tsp     ginger garlic paste
    1/4     tsp     turmeric powder
    1       tsp     coriander powder

    1       tbsp    chickpea flour
    1               tomato
    1       tsp     salt
    1/2     tsp     sugar
    2       tsp     yoghurt
    1/2     tsp     garam masala
    1/2     tsp     fenugreek leaves

for tempering:
    
    2       tsp     ghee
    1/2     tsp     cumin seeds
    2       tbsp    chopped garlic
    2               dried red chillis

## Method

Boil 1.5L water in watercooker and pour over spinach untill the spinach is soft.  
Grind half to a paste and chop other half.  

Bake in oil:  
- cumin seeds  
- green chillis  

Add onion and cook until soft.  

Add and mix well:  
- ginger garlic paste  
- turmeric  
- coriander powder  

Add chickpea flour and cook on low flame for two minutes.  
Add a little water if necessary.  

Add tomatos and cook until soft.  

Add and cook for 2 minutes:  
- chopped spinach  
- spinach puree  

Add and cook for 2 minutes:  
- salt  
- sugar  
- yoghurt  

Add and mix:  
- garam masala  
- fenugreek leaves  


Tempering:  

Bake in ghee untill garlic is golden brown:  
- cumin seeds  
- garlic  

Add red chillis and pour over palak.  
