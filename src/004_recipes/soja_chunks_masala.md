# Soy chunks masala

## Ingredients

    100 g       soy chunks
    2   tsp     ginger/garlic paste
    2           onion chopped
    12          cashews
    2           tomatos chopped

    2           bay leaves
    2           black cardamom
    1/4         mace

    1/4 cup     yoghurt

    1/2 tsp     cumin seeds
    1/2 tsp     turmeric powder
    1   tsp     red chilli powder
    1/2 tsp     cumin powder
    1/2 tsp     coriander powder
    1/2 tsp     garam masala
    1   tsp     salt

    oil
    coriander leaves
    2 cup of water
