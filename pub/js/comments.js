"use strict";

// get page title, this will become the api endpoint
const pageTitle = document.title
const getURL     = `http://localhost:5000/v0.1/pages/${pageTitle}/comments`
const postURL    = `http://localhost:5000/v0.1/pages/${pageTitle}/comments`
const captchaURL = "http://localhost:5000/v0.1/captcha"

// max reply depth
const maxReplyDepth = 5

// create a random username and save in session storage
if (! sessionStorage.getItem("username")) {
    const rnd = Number.parseInt(Math.random() * 100000)
    sessionStorage.setItem("username", "Anon" + rnd)
}

// poll for new comments every n miliseconds
const getCommentsInterval = 20000

const initApp = () => {
    document.querySelector("#formComment").addEventListener('submit', event => {
        event.preventDefault()
        doPostComment(postURL)
    })

    // query API for captcha and show in input field
    setCaptcha()

    doDisplayComments()

    // poll for new comments every n miliseconds
    //setInterval(doDisplayComments, getCommentsInterval)
    document.querySelector("#formAuthor").value = sessionStorage.getItem("username")

    // form is disabled by default so we have to enable it.
    // this prevents it from showing when javascript is disabled
    document.getElementById('hideThisFromNonJs').style.display='block';

    // reset the replyTo key which indicates the selected msg id that we're going to reply to
    sessionStorage.setItem('replyToID', '')

    // hide reply container by default
    document.getElementById('formReplyTo').style.display='none';
}

const padZero = (value) => {
    // left pad with zero
    if (value < 10)
        value = "0" + value
    return value
}

const formatDate = (dt) => {
    // re-format date to something presentable
    const date = new Date(dt)
    const [month, day, year]       = [date.getMonth(), date.getDate(), date.getFullYear()];
    const [hour, minutes, seconds] = [date.getHours(), date.getMinutes(), date.getSeconds()];
    return `${year}-${padZero(month)}-${padZero(day)} ${padZero(hour)}:${padZero(minutes)}`
}

const APIGetComments = async (url) => {
    // get all comments from API
    const data = await fetch(url)
    const json = await data.json()
    console.log("Got comments =>", json)
    return json
}

const APIPostComment = async (url, comment) => {
    // post comment to api
    console.log(comment)
    const response = await fetch(url, {
        method: 'POST',
        body: JSON.stringify(comment),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    const json = await response.json()
    return json
}

const APIGetCaptcha = async (url) => {
    // get a captcha from API
    const data = await fetch(url)
    const json = await data.json()
    return json.payload
}

const setErrorMSG = (msg) => {
    console.error(msg)
    const formStatus = document.querySelector("#formStatus")
    formStatus.textContent = msg
}

const setCaptcha = async () => {
    // set the capcha challenge in the captcha field
    const form = document.querySelector("#formComment")
    const formCaptcha = document.querySelector("#captchaChallenge")

    // get captcha and catch network errors
    let json = {}
    try {
        json = await APIGetCaptcha(captchaURL)
    } catch (err) {
        setErrorMSG(`Failed to get captcha from API: ${err.message}`)
        return
    }

    // check if we got an error from API
    if (json.error) {
        setErrorMSG(`Failed to get captcha from API: ${err.message}`)
        return
    }

    let hidden = ""

    // save captcha id in session storage
    sessionStorage.setItem("captchaId", json.id)

    // set challenge in input field
    formCaptcha.placeholder = json.challenge
    formCaptcha.value = ""
}

const getFormData = () => {
    // gather fields from comment form and return in object for posting
    const captchaSolution = document.querySelector("#captchaChallenge")
    const form = document.querySelector("#formComment")
    const formContent = document.querySelector("#formContent")
    const formAuthor = document.querySelector("#formAuthor")

    return {
        comment: {
            author: formAuthor.value,
            content: formContent.value,
            replyTo: sessionStorage.getItem('replyToID')
        },
        captchaId: sessionStorage.getItem("captchaId"),
        captchaSolution: captchaSolution.value
    }
}

const replyButtonClicked = (comment) => {
    console.log("setting up button")
    const formContent       = document.querySelector("#formContent")
    const form              = document.querySelector("#formComment")
    const formReplyToButton = document.querySelector("#formReplyToButton")
    const formReplyTo       = document.querySelector('#formReplyTo')

    formReplyToButton.textContent = `@${comment.author} X`
    form.scrollIntoView(true)
    formContent.focus()
    formReplyTo.style.display = 'flex';
    sessionStorage.setItem('replyToID', comment.id)

    // when clicked on reply name under form, remove it!
    formReplyToButton.addEventListener('click', event => {
        event.preventDefault()
        formReplyTo.style.display = 'none'
        sessionStorage.setItem('replyToID', '')
    })
}

const displayComment = (comment) => {
    // create comment from template
    const comment_el = document.createElement("div")
    comment_el.classList.add("commentContainer")
    comment_el.innerHTML = document.querySelector("#commentTemplate").innerHTML
    comment_el.querySelector(".commentDatetime").textContent = formatDate(comment.dateTime)
    comment_el.querySelector(".commentContent").textContent  = comment.content
    comment_el.querySelector(".commentID").textContent       = comment.id
    comment_el.querySelector(".commentAuthor").textContent   = comment.author

    // show person we have replied to after author field
    if (comment.parent) {
        comment_el.querySelector(".commentReplyTo").textContent = `@${comment.parent.author}`
    }

    // when reply is clicked
    comment_el.querySelector(".commentReply").addEventListener('click', (event) => {
        event.preventDefault()
        event.stopPropagation()
        replyButtonClicked(comment)
    })

    // if element has children, show triangles indicating colapsed/folded state
    if (comment.replies.length) {
        const foldIndicator = comment_el.querySelector(".commentFoldIndicator")

        foldIndicator.classList.add('childrenColapsed')
        foldIndicator.textContent = '▼ Hide'

        foldIndicator.addEventListener('click', event => {
            event.stopPropagation()
            foldIndicator.classList.toggle('childrenFolded')
            foldIndicator.classList.toggle('childrenColapsed')

            if (foldIndicator.classList.contains("childrenFolded")) {
                foldIndicator.textContent = `▶ Show ${comment_el.querySelectorAll(".commentContainer").length} comment`
                foldIndicator.textContent += (comment_el.querySelectorAll(".commentContainer").length > 1) ? "s" : ""
            } else {
                foldIndicator.textContent = '▼ Hide'
            }
        })
    }

    return comment_el
}

const recSortComments = (comments, comment=false, indent=0) => {
    // use recursion to sort comments in a tree like structure
    const curLevel = []
    const notCurLevel = []

    // get all comments from current level
    for (const c of comments) {
        if (!c.replyTo || (c.replyTo === comment.id)) {
            curLevel.push(c)
        } else {
            notCurLevel.push(c)
        }
    }

    for (const c of curLevel) {
        c.replies = recSortComments(notCurLevel, c, indent+1)
        c.indent = indent
        c.parent = comment
    }
    return curLevel
}

const recDisplayComments = (comments, indent=0, container) => {
    // use recursion to display indented comments
    const space = new Array(indent + 1).join(' ');

    for (const c of comments) {

        const comment_el = displayComment(c)

        if (container) {
            container.append(comment_el)

            comment_el.querySelector('.commentReplyTo').addEventListener('click', event => {
                event.preventDefault()
                const parent_comment = container.parentElement.querySelector(".comment")
                parent_comment.classList.add("highlight")
                parent_comment.scrollIntoView(true)

                // set timeout before removing class so the animation can play
                setTimeout(function() {
                    parent_comment.classList.remove("highlight")
                }, 2000);
            })
        } else {
            commentsView.append(comment_el)
        }

        if (c.replies.length > 0) {
            // create container element to add children to
            const new_container = document.createElement("div")
            new_container.classList.add("repliesContainer")
            new_container.classList.add('commentChild')

            // debugging
            //const colors = ['red', 'green', 'magenta', 'blue', 'yellow', 'cyan']
            //new_container.style.border = `solid ${colors[c.indent]} 5px`

            comment_el.append(new_container)
            comment_el.querySelector(".commentFoldIndicator").addEventListener('click', event => {
                event.preventDefault()
                event.stopPropagation()

                new_container.classList.toggle("colapse")
            })
            // get all children and pass new container to add them to
            recDisplayComments(c.replies, indent+1, parent=new_container)
        }
    }
}

const doDisplayComments = async () => {
    // TODO: do intelli sorting of threads by date
    const commentsView = document.querySelector("#commentsView")

    let json = []

    // get comments and catch network errors
    try {
        json = await APIGetComments(getURL)
    } catch (err) {
        commentsView.textContent = `Failed to get comments from API: ${err.message}`
        console.error(`Failed to get comments from API: ${err.message}`)
        return
    }

    // check if we got an error from API
    if (json.error) {
        commentsView.textContent = `Failed to get comments from API: ${json.error.message}`
        console.error(`Failed to get comments from API: ${json.error.message}`)
        return
    }

    if (json.payload.length == 0) {
        commentsView.textContent = "No comments"
        return
    }

    commentsView.innerHTML = ""
    recDisplayComments(recSortComments(json.payload))
}

const doPostComment = async (url) => {
    // callback for post button
    const form       = document.querySelector("#formComment")
    const formStatus = document.querySelector("#formStatus")
    const formData   = getFormData()

    // get form data
    const json = await APIPostComment(url, formData)

    // check for errors and reset fields
    if (json.error) {
        formStatus.textContent = json.error.message
        setCaptcha()
        console.error(json.error.message)
        return
    } else {
        formStatus.textContent = ""
        doDisplayComments()
        form.reset()
    }

    // reset replyTo key and hide replyTo form
    document.querySelector("#formReplyTo").style.display = 'none'
    sessionStorage.setItem('replyToID', '')

    document.querySelector("#formAuthor").value = sessionStorage.getItem("username")

    setCaptcha()
}

document.addEventListener("readystatechange", (event) => {
    if (event.target.readyState === "complete") {
        initApp()
    }
})